import sys, os, openpyxl

files = []
for file in os.listdir():
    if file[-5:] == '.xlsx':
        files.append(file)

mergedData=""
for file in files:
    wb=openpyxl.load_workbook(file)
    for sheet in wb.get_sheet_names():
        currentSheet = wb.get_sheet_by_name(sheet)
        endOfFile=False
        row = 0
        blankLineCount=0
        while not endOfFile:
            row += 1
            if currentSheet["A{0}".format(row)].value == None:
                blankLineCount += 1
                if blankLineCount > 5:
                    endOfFile = True
            elif currentSheet["A{0}".format(row)].value.lower() == 'device':
                pass
            else:
                mergedData += "{0}~{1}~{2}\n".format(currentSheet["A{0}".format(row)].value, currentSheet["B{0}".format(row)].value, currentSheet["C{0}".format(row)].value)

outputFile = "output.txt"
if len(sys.argv) > 1:
    outputFile = sys.argv[1]
with open(outputFile, "w") as output:
    output.write(mergedData)
